# init environment

import scramble

# preload some modules

import sys

# save console history

import os
import readline
import atexit
import rlcompleter

from pmp.core import get_logger

log = get_logger()

historyPath = os.path.expanduser("~/.pyhistory")

def save_history(historyPath=historyPath):
    import readline
    readline.write_history_file(historyPath)

if os.path.exists(historyPath):
    log.info("reading history from {0} ...", historyPath)
    readline.read_history_file(historyPath)

atexit.register(save_history)

# set tab behavior

# regular completion
readline.parse_and_bind("tab: complete")

# insert tabs
# readline.parse_and_bind("set disable-completion on")  
# readline.parse_and_bind("tab: self-insert")
