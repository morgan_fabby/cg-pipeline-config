function dlog()
{
    echo $(date '+%H:%M:%S')" "$1
}

dlog "Loading PMP profile ..."

function getOsPlatform()
{
    local SysName=$(uname -a | cut -d' ' -f1)
    local OsName=''
    local ec=1

    if [ "$SysName" == 'Darwin' ]; then
        OsName="darwin"
    elif [ "$SysName" == 'Linux' ]; then
        OsName="linux2"
    elif [[ "$SysName" =~ 'MINGW' ]]; then
        OsName="win32"
    elif [[ "$SysName" =~ 'CYGWIN' ]]; then
        OsName="win32"
    elif [[ "$SysName" =~ 'MSYS_NT' ]]; then
        OsName="win32"
    fi

    if [ -n "$OsName" ]; then ec=0; fi

    echo "$OsName"
    return $ec
}

function getSharedPath()
{
    local path=""

    if [ $(getOsPlatform) == "win32" ]; then
        path="$PUBLIC"
        if [ ! -e "$path" ]; then path="C:\\Users\\Public"; fi
    elif [ $(getOsPlatform) == "darwin" ]; then
        path="$HOME"
        if [ -z "$path" ]; then path="/Users/dummy"; fi
        path=$(dirname "$path")"/Shared"
        if [ ! -e "$path" ]; then path="/Users/Shared"; fi
    elif [ $(getOsPlatform) == "linux2" ]; then
        path="/var/tmp"
    fi

    echo "$path"

    if [ -e "$path" ]; then
        return 0
    else
        return 1
    fi
}

function guessScramblePath()
{
    local scramble_path

    if [ -n "$PMP_ROOT" -a -e "$PMP_ROOT/sys/mods/sh/scramble.sh" ]; then
        scramble_path="$PMP_ROOT/sys/mods/sh/scramble.sh"
    else
        local shared_path
        local dev_path
        local search_path
        local folder

        shared_path=$(getSharedPath)
        search_path="$shared_path/pipeline/pmp"

        for folder in $search_path; do
            scramble_path="$folder/sys/mods/sh/scramble.sh"
            if [ -e "$scramble_path" ]; then
                export PMP_ROOT="$folder"
                break
            fi
        done
    fi

    if [ -e "$scramble_path" ]; then
        echo "$scramble_path"
        return 0
    else
        echo ""
        return 1
    fi
}

scramble_path=$(guessScramblePath)

if [ -n "$scramble_path" ]; then
    . "$scramble_path"
else
    echo "ERROR : Failed to identify a valid scramble.sh candidate !"
fi

unset scramble_path

# ENFORCED CUSTOMS

alias ls="ls -G"
alias la="ls -lisahG"
alias df="df -h"

if [ $(getOsPlatform) == "win32" ]; then
    alias python="winpty python.exe"
fi

# QUICK CD

function pmproot()
{
    local folders="$PMP_ROOT"
    local folder

    for folder in $folders; do
        if [ -e "$folder" ]; then
            cd "$folder"
            break
        fi
    done
}

# QUICK CD

alias pmpconf='pmpgo "$SG_LOCAL/config"'
alias pmpapp='pmpgo "$PMP_ROOT/shotgun_apps/$SG_BUILD_APP"'
alias pmppy='pmpgo "$PMP_ROOT/py"'
alias pmpsys='pmpgo "$PMP_ROOT/sys"'
alias pmplogs='pmpgo $(PMPLogsPath)'
alias pmplogsg='pmpgo $(PMPShotgunLogsPath)'
alias pmpsql='mysql pmp -h thor -u pmp_service -p'
alias goshr='cd /c/Users/Public/pipeline/shared/mofa'

# QUICK TAIL LOG

alias tlogservice='tlog "shotgunEventDaemon"'
alias tlogtasks='tlog "plugin_taskAssigned"'
alias tlogdesktop='tlogshotgun "tk-desktop"'
alias tlogshell='tlogshotgun "tk-shell"'

# Python shortcuts

alias py2='winpty C:/Python27/python.exe'
alias py2o='winpty C:/Python27/python.exe -O'
alias py3='winpty C:/Python37/python.exe'
alias py3o='winpty C:/Python37/python.exe -O'

# COMPLETION FUNCTIONS

function _SGConfigsComplete
{
    local ConfRoot=$(dirname "$PMP_ROOT")/shotgun
    local Configs=$(ls "$ConfRoot")

    COMPREPLY=()
    cur=${COMP_WORDS[COMP_CWORD]}

    case "$cur" in
    *)
        COMPREPLY=( $( compgen -W "$Configs" -- $cur ) );;
    esac
    return 0
}

# DEV PYTHON SCRIPTS

function bins_deploy() { python "$PMP_ROOT/py/scripts/dev/bins_deploy.py" "$@"; }
function build() { python "$PMP_ROOT/py/scripts/dev/build.py" "$@"; }
function deploy() { python "$PMP_ROOT/py/scripts/deploy.py" "$@"; }
function check_doc() { python "$PMP_ROOT/py/scripts/dev/check_doc.py" "$@"; }
function generate_doc() { python "$PMP_ROOT/py/scripts/dev/generate_doc.py" "$@"; }
function consolidate_swarm() { python "$PMP_ROOT/py/scripts/dev/consolidate_swarm.py" "$@"; }
function check_line_endings() { python "$PMP_ROOT/py/scripts/dev/check_line_endings.py" "$@"; }
function rearm_garm() { python "$PMP_ROOT/py/scripts/dev/rearm_garm.py" "$@"; }
function list_swarm_hosts() { python "$PMP_ROOT/py/scripts/dev/list_swarm_hosts.py" "$@"; }

function clean_git_locks() {
    cd "$PMP_ROOT/.git";
    rm -v `find . -name "index\.lock"`;
    cd -;
}

function run_unit_tests() {
    python -m bagpipe.test
    py2 -m bagpipe.test
    py3 -m bagpipe.test
}

function pgd() { python "$PMP_ROOT/py/scripts/pgd.py" "$@"; }

# SHOTGUN PYTHON SCRIPTS

function deployed_configs() { python "$PMP_ROOT/py/scripts/sg/deployed_configs.py" "$@"; }
function grab_configs() { python "$PMP_ROOT/py/scripts/sg/grab_configs.py" "$@"; }
function clean_configs() { python "$PMP_ROOT/py/scripts/sg/clean_configs.py" "$@"; }

function generate_schema() { python "$PMP_ROOT/py/scripts/dev/generate_schema.py" "$@"; }
function check_shotgun_updates() { python "$PMP_ROOT/py/scripts/sg/check_shotgun_updates.py" "$@"; }
function spread_config_files(){ python "$PMP_ROOT/py/scripts/sg/spread_config_files.py" "$@"; }

complete -F _SGConfigsComplete -o filenames clean_configs
complete -F _SGConfigsComplete -o filenames deploy

# USER CUSTOM PROFILE

profile_custom="$HOME/.profile.custom"

if [ -e "$profile_custom" ]; then
    . "$profile_custom"
fi

unset profile_custom

# CD TO CONFIGURED STATUP FOLDER

if [ -e "$SHELL_STARTUP_DIR" ]; then
    cd "$SHELL_STARTUP_DIR"
fi
