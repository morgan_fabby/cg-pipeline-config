$pmp_root = $Env:PMP_ROOT;
$Env:PSModulePath = $Env:PSModulePath + ";" + $pmp_root + "/sys/mods/ps"

Import-Module -Name PMPCore -Global
Import-Module -Name PMPCvs -Global
Import-Module -Name SGCore -Global
Import-Module -Name PMPBuild -Global

PMPAddPath PATH ($Env:PMP_ROOT + "\sys\scripts")

function backup_work()
{
	PMPShell "backup_work" 1
}
